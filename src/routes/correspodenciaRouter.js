//import auth from "../middlewares/auth";
import routerx from "express-promise-router";
import correspodenciaController from "../controllers/correspodenciaController";
import auth from "../middlewares/auth";

const router = routerx();


router.post('/',/**  auth.verifySuper,*/ correspodenciaController.add)
router.get("/", /** auth.verifyUsuario,*/ correspodenciaController.list);
router.post("/upload/:id",/**auth.verifyUsuario,*/ correspodenciaController.upload);
router.get('/retrieve/:arquivo/:token', correspodenciaController.download)
router.get('/:id', /*auth.verifyAdmin,*/ correspodenciaController.findById)



export default router;
