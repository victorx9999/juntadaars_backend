'use strict';

module.exports = (sequelize, DataTypes) => {
    const AnexoCorrespondencia = sequelize.define('AnexoCorrespondencia', {
        arquivo: DataTypes.STRING,
        observacoes: DataTypes.TEXT,
        correspodenciaId: DataTypes.INTEGER
    }, {
        freezeTableName: true
    });

    AnexoCorrespondencia.associate = function (models) {
        AnexoCorrespondencia.belongsTo(models.Correspodencia, { as: 'correspodencias', foreignKey: 'correspodenciaId' })
    };

    return AnexoCorrespondencia;
};