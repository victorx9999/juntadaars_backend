'use strict';

module.exports = (sequelize, DataTypes) => {
    const Correspodencia = sequelize.define('Correspodencia', {
        numero_processo: DataTypes.STRING,
        destinatario: DataTypes.STRING,
        data: DataTypes.STRING,
        usuario_id: DataTypes.INTEGER,

    }, {
        freezeTableName: true,

        tableName: 'correspodencia'
    });

    Correspodencia.associate = function (models) {

        Correspodencia.belongsTo(models.Usuario, { as: 'usuarios', foreignKey: 'usuario_id' })


    };

    return Correspodencia;
};