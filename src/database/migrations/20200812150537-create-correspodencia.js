'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('correspodencia', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      numero_processo: {
        allowNull: false,
        type: Sequelize.STRING
      },
      destinatario: {
        allowNull: false,
        type: Sequelize.STRING
      },


      data: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: false
      },

      usuario_id: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'usuarios',
          key: 'id'
        }
      },

      created_at: {
        allowNull: false,
        defaultValue: Sequelize.fn('NOW'),
        type: Sequelize.DATE
      },

      updated_at: {
        allowNull: false,
        defaultValue: Sequelize.fn('NOW'),
        type: Sequelize.DATE
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('correspodencia');
  }
};
